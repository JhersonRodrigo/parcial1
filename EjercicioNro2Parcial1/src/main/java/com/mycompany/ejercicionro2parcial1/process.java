/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ejercicionro2parcial1;

/**
 *
 * @author Jherson Rodrigo Mamani Poma - 10577476
 */
public class process {
    String mycad;
    pila stackval = new pila();
    
    public process(String str){
        this.mycad = str;
    }
    
    public void checkStackVal(String strs){   
        for(int i = 0;i<strs.length();i++ ){
            if(strs.charAt(i) == '('){
                stackval.push(')');
            }else if(strs.charAt(i) == '['){
                stackval.push(']');
            }else if(strs.charAt(i) == ']'){
                checkStack(']','[');
            }else if(strs.charAt(i) == ')'){
                checkStack(')','(');
            }
        }
        
        if(stackval.isEmpty()){
            System.out.println("Esta Balanceado");
        }else{
            System.out.println("No esta balanceado");
        }
    }
   
    public void checkStack(char c, char cc){
        if(stackval.isEmpty()){
            System.out.println("No esta balanceado" + cc);
            System.exit(0);
        }else{
            char s = stackval.pop();
            if(c != s){
                System.out.println("No esta balanceado" + s);    
                System.exit(0);
            }
        }
    } 

}
