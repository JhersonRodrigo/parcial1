/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ejercicionro1parcial1;

/**
 *
 * @author Jherson Rodrigo Mamani Poma - 10577476
 */
public class pila {
    int size;
    int arr[];
    int top;
    
    pila(int size){
        this.size = size;
        this.arr = new int[size];
        this.top = -1;
    }

    pila() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public boolean isFull(){
        return (size-1 == top);
    }
    
    public boolean isEmpty(){
        return (top == -1);
    }
    
    public void push(int element){
       if(!isFull()){
           top++;
           arr[top] = element;
       }else{
           System.out.println("la pila esta llena");
       }
    }
    
    public int pop(){
        if(!isEmpty()){
            int returntop = top;
            top--;
            int temp = arr[returntop];
            return temp;
        }
        else{
            System.out.println("la pila esta vacia");
            return -1;
        }
    }
    
    public int peek(){
        return arr[top];
    }
  
}
    
