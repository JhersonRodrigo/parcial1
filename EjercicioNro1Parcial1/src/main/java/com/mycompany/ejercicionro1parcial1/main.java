
package com.mycompany.ejercicionro1parcial1;

import javax.swing.JOptionPane;


/**
 *
 * @author Jherson Rodrigo Mamani Poma - 10577476
 */
public class main {
    public static void main(String[]args){
        String inputText,inputSelect,inputData,inputType;
        inputText = JOptionPane.showInputDialog("Tamaño de la pila:");
        inputSelect = JOptionPane.showInputDialog("Que tipo de pila desea crear: \n 1. Cadena \n 2. Enteros \n 3. Ambos");
        int count = 1;
        int limit = Integer.parseInt(inputText);
        String result = "";
        switch(inputSelect){
            /*case "1":
                pila pd = new pila();
                do{
                    inputData = JOptionPane.showInputDialog("Introduzca una cadena");
                    if(inputData.length() == 0){
                        inputData = JOptionPane.showInputDialog("Introduzca una cadena");    
                    }else{
                        pd.push(Integer.parseInt(inputData));
                    }
                    count++;
                }while(count <= limit);
                inputType = JOptionPane.showInputDialog("Ordenar: \n -ascendente \n -descendente");
                pila resultStackDinamic = sortStackDinamic(pd,inputType);
                while(!resultStackDinamic.isEmpty()){
                    result = result+ " - " + resultStackDinamic.pop();
                }
                JOptionPane.showMessageDialog(null, result);
            break;*/
            case "2":
                pila ps = new pila(limit);
                do{
                    inputData = JOptionPane.showInputDialog("Introduzca un numero entero");
                    if(inputData.length() == 0){
                        inputData = JOptionPane.showInputDialog("Introduzca un numero entero");    
                    }else{
                        ps.push(Integer.parseInt(inputData));
                    }
                    count++;
                }while(count <= limit);
                inputType = JOptionPane.showInputDialog("ordenar: \n -ascendente \n -descendente");
                pila resultStackStatic = sortStackStatic(ps,inputType);
                while(!resultStackStatic.isEmpty()){
                    result = result+ " - " + resultStackStatic.pop();
                }   
                JOptionPane.showMessageDialog(null, result);
            break;
            default:
                JOptionPane.showMessageDialog(null, "Elija una opcion valida");
            break;
        }
    }
    
    public static pila sortStackStatic(pila p,String type){
        pila tp = new pila(10);
        while(!p.isEmpty()){
            int currentData = p.pop();
            if(type == "ascendente"){
                while(!tp.isEmpty() && tp.peek() > currentData){
                    p.push(tp.pop());
                }
                tp.push(currentData); 
            }else{
                while(!tp.isEmpty() && tp.peek() < currentData){
                    p.push(tp.pop());
                }
                tp.push(currentData);       
            }
        }
        return tp;
    }
    
    public static pila sortStackDinamic(pila p,String type){
        pila tp = new pila();
        while(!p.isEmpty()){
            int currentData = p.pop();
            while(!tp.isEmpty() && tp.peek() > currentData){
                p.push(tp.pop());
            }
            tp.push(currentData);
        }
        return tp;
    }
    
}
